import {Nav} from "tabler-react";
import {
  Link
} from "react-router-dom";

export const NavBar = (props) => {
    return (
        <>
        <Nav className="p-sm-3 pl-md-5">
            <Nav.Item hasSubNav value="Home" icon="globe">
                <Nav.SubItem value="Task #1" to="/task/1" >
                   Task #1
                </Nav.SubItem>
            </Nav.Item>
            <Nav.Item icon="user" to="/users">
                Users
            </Nav.Item>
        </Nav>
            {props.child()}
            </>
    );
};
