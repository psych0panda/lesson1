import {Button, Card, Container, Table} from "tabler-react";
import users from '../guests.json';
import "tabler-react/dist/Tabler.css";
import {useEffect, useState} from "react";
import { Form } from "tabler-react";

export const TableUsers = () => {
    const [usersData, setUsers] = useState([]);
    const [arrivals, setArrivals] = useState([]);
    const [search, setSearch] = useState("");
    const [searchUsers, setSearchUsers] = useState([]);

    const handlerSearch = (e) => {
      setSearch(e.target.value);
      if (search.length === 1) {
          setUsers(searchUsers);
      } else {
          setUsers(usersData.filter((item) => item.name.includes(search),))
      }
    }

    const handlerClick = (e, obj) => {
        let idx = usersData.findIndex(curr_val=>curr_val._id===obj);
        if (idx !== -1) {
            let tmp = {...usersData[idx], 'state': true}
            setUsers(usersData.filter(i => i.index!==tmp['index']));
            setSearchUsers(usersData.filter(i => i.index!==tmp['index']));
            setArrivals([tmp]);
        }
    }

    useEffect(() =>{
        setUsers(users.map(usr => Object.assign(usr, {'state': false})));
    },[]);

    return (
        <>
            <Container>
                <Card className="p-sm-3 mt-md-3">
                    <label htmlFor="search">
                        Search by name:
                        <Form.Input placeholder="Search by Username" onChange={handlerSearch}/>
                    </label>
                    <Table>
                        <Table.Header>
                            <Table.ColHeader>Index</Table.ColHeader>
                            <Table.ColHeader>Name</Table.ColHeader>
                            <Table.ColHeader>Company</Table.ColHeader>
                            <Table.ColHeader>Email</Table.ColHeader>
                            <Table.ColHeader>Phone</Table.ColHeader>
                            <Table.ColHeader>Address</Table.ColHeader>
                        </Table.Header>
                        <Table.Body>
                            {usersData.map(user =>
                                <Table.Row key={user._id.toString()}>
                                    <Table.Col>{user.index}</Table.Col>
                                    <Table.Col>{user.name}</Table.Col>
                                    <Table.Col>{user.company}</Table.Col>
                                    <Table.Col>{user.email}</Table.Col>
                                    <Table.Col>{user.phone}</Table.Col>
                                    <Table.Col>{user.address}</Table.Col>
                                    <Table.Col>
                                        <Button className="btn-facebook" value={true} onClick={e => handlerClick(e, user._id)} >Прибыл</Button>
                                    </Table.Col>
                                </Table.Row>
                            )}
                        </Table.Body>
                    </Table>
                </Card>
            </Container>
        </>
    );
};