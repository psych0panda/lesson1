import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {Home} from "./components/Home";
import {TableUsers} from "./components/Table";
import {NavBar} from "./components/NavBar";


function App() {
    const UsersPage = () => <TableUsers />;
    const HomePage = () => <Home/>;
    return (
        <>
            <Router>
                <Switch>
                    <Route exact path="/">
                        <NavBar child={HomePage}/>
                    </Route>
                    <Route exact path="/task/:task_id">
                        <NavBar child={HomePage}/>
                    </Route>
                    <Route exact path="/users">
                        <NavBar child={UsersPage}/>
                    </Route>
                </Switch>
            </Router>
        </>
    );
}

export default App;
